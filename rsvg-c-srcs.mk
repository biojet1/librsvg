librsvg_public_headers =		\
	include/librsvg/rsvg.h		\
	include/librsvg/rsvg-cairo.h	\
	include/librsvg/rsvg-features.h \
	include/librsvg/rsvg-version.h	\
	$(NULL)

librsvg_private_headers =	 	\
	include/librsvg/rsvg-css.h	\
	$(NULL)

rsvg_convert_srcs = rsvg-convert.c
